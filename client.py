#!/usr/bin/env python

import zmq
from zmq import ssh

import sys

ctx = zmq.Context()

if __name__=="__main__":
    server_connection_string = sys.argv[1]

    s = ctx.socket(zmq.REQ)

    ssh.tunnel_connection(s, 'tcp://127.0.0.1:9090', server_connection_string)

    s.send_string("Hello, World")
    reply = s.recv_string()

    print(reply)
