# zmq-tunnel-echo #

Super-simple example of an echo server and a client. Server and client may be on different machines, but since a SSH tunnel is used, it feels like the same machine and the connection is encrypted. All common SSH authentications may be used. [More information can be found here.](http://zeromq.github.io/pyzmq/ssh.html)

## Dependencies ##

I used `virtualenv` with Python 3, in addition I had to install `pexpect`. The linked documentation above states that it is not available for Python 3, but this information seems to be outdated.

## Usage ##

On the server side, do

    $ ./server.py # use Ctrl-X to interrupt

On the client:

    $ ./client.py connection-string

... and replace `connection-string` with something appropriate, for example `user@remote-machine`. If you enabled public key authentication and entered the password, the client should now send "Hello, World" and the server should print this and reply the same.