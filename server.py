#!/usr/bin/env python
import zmq

from zmq.eventloop import ioloop

loop = ioloop.IOLoop.instance()

ctx = zmq.Context()
s = ctx.socket(zmq.REP)
s.bind('tcp://127.0.0.1:9090')

def rep_handler(sock, events):
    msg = sock.recv_string()
    print("GOT:", msg)
    sock.send_string(msg)

loop.add_handler(s, rep_handler, zmq.POLLIN)

loop.start()
